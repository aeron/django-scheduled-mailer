# coding: utf-8
from django.conf import settings
from django.contrib import admin
from django import forms
from django.contrib.auth.models import User
from scheduled_mailer.models import Message, DontSendEntry, MessageLog
from django.utils.encoding import force_unicode
# from lxml import html
# from scheduled_mailer import send_html_mail
from scheduled_mailer import send_mail


class MessageAdminForm(forms.ModelForm):
	subject = forms.CharField(label=u"Тема", required=True)
	text = forms.CharField(label=u"Текст", widget=forms.Textarea(), required=True)

	class Meta:
		model = Message
		fields = ('subject', 'text', 'when_send', 'priority')


class MessageAdmin(admin.ModelAdmin):
	list_display = ["id", "to_addresses", "subject", "when_added", "priority"]
	form = MessageAdminForm

	def save_model(self, request, obj, form, change):
		subject = force_unicode(form.cleaned_data['subject'])
		message = force_unicode(form.cleaned_data['text'])
		# message_html = unicode(html.fromstring(form.cleaned_data['text']).text_content())
		from_email = settings.DEFAULT_FROM_EMAIL
		recipient_list = set(User.objects.values_list('email', flat=True))

		for recipient in recipient_list:
			# send_html_mail(subject, message, message_html, from_email, [recipient])
			send_mail(subject, message, from_email, [recipient])


class DontSendEntryAdmin(admin.ModelAdmin):
	list_display = ["to_address", "when_added"]


class MessageLogAdmin(admin.ModelAdmin):
	list_display = ["id", "to_addresses", "subject", "when_attempted", "result"]


admin.site.register(Message, MessageAdmin)
admin.site.register(DontSendEntry, DontSendEntryAdmin)
admin.site.register(MessageLog, MessageLogAdmin)
