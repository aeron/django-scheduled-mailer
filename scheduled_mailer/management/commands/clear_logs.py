# coding: utf-8
import logging
from django.core.management.base import NoArgsCommand
from scheduled_mailer.models import MessageLog

logging.basicConfig(level=logging.DEBUG, format="%(message)s")


class Command(NoArgsCommand):
	help = "Simple log cleaner, just wipeout log table."

	def handle_noargs(self, **options):
		logs = MessageLog.objects.all()
		count = logs.count()
		logs.delete()
		logging.info("%d cleaned." % count)
